
%.Titulo: Trabajo Práctico 2 - Recomendador de canciones  
%.Integrantes: Ignacio Cattoni - Luis Dzikiewicz
%.Breve descripción del tema.
%.El objetivo del sistema es recomendar distintas canciones 
% según género, subgenero, artista o album.
%.
%.Reglas de clasificación
%.
    %.Rock
        %. Heavy Metal
            %. Enter Sandman es de Rock, parte del subgenero heavy metal y pertenece a la banda Metallica.
            %. Master of Puppets, es de Rock, pertenece a la banda Metallica y pertenece al album homonimo.
            %. The Trooper es de Rock, parte del subgenero heavy metal y pertenece al album Piece Of Mind.
            %. Run to the hills es una cancion parte del subgenero heavy metal, perteneciente al album Number of the Beast, de la banda Iron Maiden.
        %. Hard Rock
            %. Smoke on the Water fue creada por Deep Purple, pertenece al sub genero del Hard Rock 
            %. Perfect Strangers fue creada por Deep Purple para el album del mismo nombre y pertenece al sub genero del Hard Rock
            %. Stairway to Heaven fue compuesta por la banda de hard rock Led Zepellin para el disco Led Zepellin IV.
            %. Inmigrant song fue compuesta por la banda Led Zepellin, pertenece al genero hard rock, para el disco Led Zepellin III.
    %.Latino
        %. Cumbia
            %. Curiosidad es una canción latina de cumbia, y pertenece al disco "De fiesta" de Rombai.
            %. Me voy es una canción latina de cumbia, y es un sencillo de Rombai.
            %. Menea para mí es una canción latina de cumbia, y pertenece al disco "100% negro cumbiero" de Damas Gratis.
            %. Alza las manos es una canción latina de cumbia, y pertenece al disco "Operación" de Damas Gratis.
        %. Cuarteto
            %. Intento es una canción latina de cuarteto, y pertenece al disco "Soy" de Ulises Bueno.
            %. Ahora mírame es una canción latina de cuarteto, y pertenece al disco "No me pidan que baje el volumen" de Ulises Bueno.
        %. Reggaeton
            %. Gasolina es una canción latina de reggaeton, y pertenece al disco "Barrio fino" de Daddy Yankee.
            %. Pasarela es una canción latina de reggaeton, y pertenece al disco "Prestige" de Daddy Yankee.
            %. Conteo es una canción latina de reggaeton, y pertenece al disco "King of kings" de Don Omar.
            %. Sexy robótica es una canción latina de reggaeton, y pertenece al disco "iDon" de Don Omar.
            %. Borro cassette es una canción latina de reggaeton, y pertenece al disco "Pretty Boy, Dirty Boy" de Maluma.
            %. El préstamo es una canción latina de reggaeton, y pertenece al disco "F.A.M.E." de Maluma.
    %. Pop
        %. Pop Latino
            %. La tortura es una canción pop latina, y pertenece al disco "Oral Fixation vol. 1" de Shakira.
            %. Hips don't lie es una canción pop latina, y pertenece al disco "Oral Fixation vol. 2" de Shakira.
            %. Loba es una canción pop latina, y pertenece al disco "Loba" de Shakira.
            %. Livin la vida loca es una canción pop latina, y pertenece al disco "Ricky Martin" de Ricky Martin.
            %. Tu recuerdo es una canción pop latina, y pertenece al disco "MTV Unplugged" de Ricky Martin.
            %. Al filo de tu amor es una canción pop latina, y pertenece al disco "Vives" de Carlos Vives.
            %. Volvi a nacer es una canción pop latina, y pertenece al disco "Corazón profundo" de Carlos Vives.
        %. Dance Pop
            %. Judas es una canción dance pop, y pertenece al disco "Born this way" de Lady Gaga.
            %. Applause es una canción dance pop, y pertenece al disco "Artpop" de Lady Gaga.
            %. Baby one more time es una canción dance pop, y pertenece al disco "Baby one more time" de Britney Spears.
            %. Toxic es una canción dance pop, y pertenece al disco "In the zone" de Britney Spears.
            %. Oops!... I did it again es una canción dance pop, y pertenece al disco "Oops!... I did it again" de Britney Spears.
            %. Party in the USA es una canción dance pop, y pertenece al disco "The time of our lives" de Miley Cyrus.
            %. We can't stop es una canción dance pop, y pertenece al disco "Bangerz" de Miley Cyrus.

%genero

isRock :- verifyMusicalGender(rock), !. 
isLatino :- verifyMusicalGender(latino), !.
isPop :- verifyMusicalGender(pop), !.

%subgenero

isHardRock :- isRock, verifyMusicalSubGender(hardRock), !.
isHeavyMetal :- isRock, verifyMusicalSubGender(heavyMetal), !.

isCumbia :- isLatino, verifyMusicalSubGender(cumbia), !.
isCuarteto :- isLatino, verifyMusicalSubGender(cuarteto), !.
isReggaeton :- isLatino, verifyMusicalSubGender(reggaeton), !.

isDancePop :- isPop, verifyMusicalSubGender(dancePop), !.
isPopLatino :- isPop, verifyMusicalSubGender(popLatino), !.

%interprete

interpreter(NombreInterprete) :- verifyInterpreter(NombreInterprete), !.

%album

album(NombreAlbum) :- verifyAlbum(NombreAlbum), !.

%Canciones

%Rock

%Hard Rock
smokeOnTheWater :- isHardRock,  interpreter(deepPurple), album(machineHead).
perfectStrangers :- isHardRock, interpreter(deepPurple),  album(perfectStrangers).
stairwayToHeaven :- isHardRock, interpreter(ledZepellin), album(ledZepellinIV).
inmigrantSong    :- isHardRock, interpreter(ledZepellin), album(ledZepellinIII).

%Heavy Metal
theTropper :- isHeavyMetal, interpreter(ironMaiden), album(pieceOfMind).
enterSandman    :-  isHeavyMetal, interpreter(metallica), album(theBlackAlbum).
masterOfPuppets :-  isHeavyMetal, interpreter(metallica), album(masterOfPuppets).
runToTheHills    :- isHeavyMetal, interpreter(ironMaiden), album(numberOfTheBeast).
%Latino

%Cumbia

curiosidad :- isCumbia, interpreter(rombai), album(deFiesta).
meVoy :- isCumbia, interpreter(rombai), album(sencillos).
meneaParaMi :- isCumbia, interpreter(damasGratis), album(cienporcientoNegroCumbiero).
alzaLasManos :- isCumbia, interpreter(damasGratis), album(operacion).

%Cuarteto

intento :- isCuarteto, interpreter(ulisesBueno), album(soy).
ahoraMirame :- isCuarteto, interpreter(ulisesBueno), album(noMePidanQueBajeElVolumen).

%Reggaeton

gasolina :- isReggaeton, interpreter(daddyYankee), album(barrioFino).
pasarela :- isReggaeton, interpreter(daddyYankee), album(prestige).
conteo :- isReggaeton, interpreter(donOmar), album(kingOfKings).
sexyRobotica :- isReggaeton, interpreter(donOmar), album(iDon).
borroCassette :- isReggaeton, interpreter(maluma), album(prettyBoyDirtyBoy).
elPrestamo :- isReggaeton, interpreter(maluma), album(fame).

%Pop

%Pop Latino

laTortura :- isPopLatino, interpreter(shakira), album(oralFixationvol1).
hipsDontLie :- isPopLatino, interpreter(shakira), album(oralFixationvol2).
loba :- isPopLatino, interpreter(shakira), album(loba).
livinLaVidaLoca :- isPopLatino, interpreter(rickyMartin), album(rickyMartin).
tuRecuerdo :- isPopLatino, interpreter(rickyMartin), album(mtvUnplugged).
alFiloDeTuAmor :- isPopLatino, interpreter(carlosVives), album(vives).
volviANacer :- isPopLatino, interpreter(carlosVives), album(corazonProfundo).

%Dance pop
judas :- isDancePop, interpreter(ladyGaga), album(bornThisWay).
applause :- isDancePop, interpreter(ladyGaga), album(artpop).
babyOneMoreTime :- isDancePop, interpreter(britneySpears), album(babyOneMoreTime).
toxic :- isDancePop, interpreter(britneySpears), album(inTheZone).
oopsIDidItAgain :- isDancePop, interpreter(britneySpears), album(oopsIDidItAgain).
partyInTheUSA :- isDancePop, interpreter(mileyCyrus), album(theTimeOfOurLives).
weCantStop :- isDancePop, interpreter(mileyCyrus), album(bangerz).

% how to ask questions
verify(S, NextQuestion) :-
(yes(S)
->
true ;
(no(S)
->
fail ;
ask(S, NextQuestion))).

verifyMusicalGender(S) :- verify(S, 'Do you like '); fail.
verifyMusicalSubGender(S) :- verify(S, 'Would you like to listen to '); fail.
verifyInterpreter(S) :- verify(S, 'May it be played by '); fail.
verifyAlbum(S) :- verify(S, 'Do you like the album '); fail.

ask(Option, Question) :- write(Question), write(Option),
write('? '),
read(Response),
nl,
( (Response == yes ; Response == y)
->
assert(yes(Option)) ;
assert(no(Option)), fail).
:- dynamic yes/1,no/1.

undo :- retract(yes(_)),fail.
undo :- retract(no(_)),fail.


%Start with this predicate.
recommendSong :- ((isSong(Song),!,
write('I recommend you to listen to the song '),
write(Song),
write('. I hope you enjoy it!'),
nl);
(write('Ups, looks like there are no songs to offer to you, sorry... Please try again!'),
nl)),
undo.

isSong(theTropper) :- theTropper, !.
isSong(enterSandman)     :- enterSandman, !.
isSong(masterOfPuppets)  :- masterOfPuppets, !.
isSong(runToTheHills)    :- runToTheHills, !.

isSong(perfectStrangers) :- perfectStrangers, !.
isSong(stairwayToHeaven) :- stairwayToHeaven, !.
isSong(smokeOnTheWater) :- smokeOnTheWater, !.
isSong(inmigrantSong)    :- inmigrantSong, !.

%Latino

%Cumbia

isSong(curiosidad         ) :- curiosidad, !.
isSong(meVoy              ) :- meVoy, !.
isSong(meneaParaMi        ) :- meneaParaMi, !.
isSong(alzaLasManos       ) :- alzaLasManos, !.

%Cuarteto

isSong(intento    ) :- intento ,!.
isSong(ahoraMirame) :- ahoraMirame,!.

%Reggaeton

isSong(gasolina        ) :- gasolina , !.
isSong(pasarela        ) :- pasarela, !.
isSong(conteo          ) :- conteo, !.
isSong(sexyRobotica    ) :- sexyRobotica, !.
isSong(borroCassette   ) :- borroCassette, !.
isSong(elPrestamo      ) :- elPrestamo, !.

%Pop

%Pop Latino

isSong(laTortura      ) :- laTortura, !.
isSong(hipsDontLie    ) :- hipsDontLie , !.
isSong(loba           ) :- loba, !.
isSong(livinLaVidaLoca) :- livinLaVidaLoca, !.
isSong(tuRecuerdo     ) :- tuRecuerdo, !.
isSong(alFiloDeTuAmor ) :- alFiloDeTuAmor, !.
isSong(volviANacer    ) :- volviANacer, !.

%Dance pop
isSong(judas          ) :- judas,!.
isSong(applause       ) :- applause,!.
isSong(babyOneMoreTime) :- babyOneMoreTime,!.
isSong(toxic          ) :- toxic,!.
isSong(oopsIDidItAgain) :- oopsIDidItAgain,!.
isSong(partyInTheUSA  ) :- partyInTheUSA,!.
isSong(weCantStop     ) :- weCantStop,!.


/* Ejemplos
 * 
 * ***Respuesta donde recomienda canción:
 * 
 * recommendSong.
 * yes.
 * Do you like rock? 
 * |: no.
 * Would you like to listen to heavyMetal? 
 * |: yes.
 * Would you like to listen to hardRock? 
 * |: no.
 * May it be played by deepPurple? 
 * |: yes.
 * May it be played by ledZepellin? 
 * |: yes.
 * Do you like the album ledZepellinIV? 
 * I recommend you to listen to the song stairwayToHeaven. I hope you enjoy it!
 * 
 * *** Respuesta donde no canción que satisfaga las condiciones:
 *  
 * guessSong.
 * no.
 * Do you like rock? 
 * |: yes.
 * Do you like latino? 
 * |: yes.
 * Would you like to listen to cumbia? 
 * |: yes.
 * May it be played by rombai? 
 * |: no.
 * Do you like the album deFiesta? 
 * |: no.
 * Do you like the album sencillos? 
 * |: no.
 * May it be played by damasGratis? 
 * |: no.
 * Would you like to listen to cuarteto? 
 * |: no.
 * Would you like to listen to reggaeton? 
 * |: no.
 * Do you like pop? 
 * Ups, looks like there are no songs to offer to you, sorry... Please try again!
 * 
 * Nota: debido a la versión de prolog utilizada (v 7.6.4), durante la ejecución del programa las preguntas se imprimen
 * luego de la respuesta. 
 */