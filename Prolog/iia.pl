/*
 *   Comandos//info util
 * En swipl:
 * 	Compilar un archivo: ?- [nombre_archivo].
 * 	Probar un predicado: ?- predicado(p1,p2,...,pn).
 *  Donde estoy parado: pwd.
 *  Variables en consultas: habitacion(X). Imprime la primer habitacion, si ingresamos ; lista la proxima habitacion,
 *  y asi sucesivamente hasta salir.
 *  Los predicados no son conmutativos. El orden importa.
 * 
 *  Busquedas en prolog funcionan a traves de 
 *  matcheo de patrones en primera instancia (cabecera).
 * 
 *  habitacion(cocina), comida(cereles) --> Primero busca el pred habitacion, 
 *  si lo encuentra busca cocina, si lo encuentra.... etc. 
 * 
 *  Que devuelva false no significa que sea falso, sino que no se 
 *  puede probar que sea verdadero utilizando al bc (base de conocimiento).
 * 
 *  Puertos para llegar a la meta: Call, exit, fail, redo
 * 
 *  Consultas complejas: son combinacion de consultas simples a traves del and. Por ej, ubicacion(X, cocina), comida(X).
 *
 *  Traceando una consulta:
 *     [trace]  ?- ubicacion(X, cocina), comida(X).
 *       Call: (8) ubicacion(_G7039, cocina) ? creep 
 *       Exit: (8) ubicacion(manzana, cocina) ? creep
 *       Call: (8) comida(manzana) ? creep
 *       Exit: (8) comida(manzana) ? creep
 *	    X = manzana ;
 *       Redo: (8) ubicacion(_G7039, cocina) ? creep
 *       Exit: (8) ubicacion(lavadora, cocina) ? creep
 *       Call: (8) comida(lavadora) ? creep
 *       Fail: (8) comida(lavadora) ? creep
 *       Redo: (8) ubicacion(_G7039, cocina) ? creep
 *       Exit: (8) ubicacion(cereales, cocina) ? creep
 *       Call: (8) comida(cereales) ? creep
 *       Exit: (8) comida(cereales) ? creep
 *	    X = cereales.
 *
 *  El resultado de la evaluacion de una consulta puede depender del orden de los predicados en la bc.
 */
 
%Predicado de habitacion
habitacion(oficina).
habitacion(baño).
habitacion(cocina).
habitacion(dormitorio).
habitacion(garage).
habitacion(comedor).
%          habitacion

%Predicado que permite ubicar objetos en una ubicacion determinada
ubicacion(escritorio, oficina).
ubicacion(manzana, cocina).
ubicacion(luz_escritorio, escritorio).
ubicacion(lavadora, cocina).
ubicacion(control_remoto, 'tv smart').
ubicacion('tv smart', comedor).
ubicacion(cereales, cocina).
ubicacion(computadora, oficina).
ubicacion(cereales, comedor).
ubicacion(miel, comedor).
%		  objeto	  ubicacion

%Predicado que representa las conexiones entre las habitaciones
puerta(comedor, oficina).
puerta(comedor, garage).
puerta(comedor, cocina).
puerta(dormitorio, comedor).
puerta(baño, dormitorio).
%      habitacion1, habitacion2
	
%Predicado que determina que objeto es una comida		
comida(manzana).
comida(cereales).
comida(miel).
%      comida

%Predicado que representa donde se encuentra el sujeto.
here(oficina).
%    ubicacion

% donde_comer(Objeto,Lugar):-ubicacion(Objeto,Lugar),comida(Objeto).

/*
list_objetos(Lugar) :- ubicacion(Objeto,Lugar),tab(2),write('Objeto: '),
                       write(Objeto),tab(5),write('Habitacion: '),
                       write(Lugar),nl,fail.

list_puertas(Lugar) :- puerta(Lugar, X), tab(2), write(X), nl, fail.

info :- here(Lugar), write('Estas en la habitación: '), write(Lugar), nl,
        write('Cosas dentro de la habitación: '), nl, list_objetos(Lugar),
        nl, write('Puedes ir a:'), nl, list_puertas(Lugar).
        
Falla porque las puertas no son conmutativas, es decir, las puertas
se definieron como entrada o salida de una habitación, pero no ambas.
*/

conexion(X, Y) :- puerta(X, Y).
conexion(X, Y) :- puerta(Y, X).

list_objetos(Lugar) :- ubicacion(X, Lugar), tab(2), write(X), nl.

list_puertas(Lugar) :- conexion(Lugar, X), tab(2), write(X), nl, fail.

info :- here(Lugar), write('Estas en la habitación: '), write(Lugar), nl,
        write('Cosas dentro de la habitación: '), nl, list_objetos(Lugar),
        nl, write('Puedes ir a:'), nl, list_puertas(Lugar).

cel_a_far(C, F) :- F is C * 9 / 5 + 32.

moverse(Lugar) :- puedo_ir(Lugar), mover(Lugar), info.

puedo_ir(Lugar) :- here(X), conexion(X, Lugar).
puedo_ir(Lugar) :- write('No puedes ir hasta la habitación'), tab(1),
                   write(Lugar), tab(1),write('desde aquí'), nl, fail.

mover(Lugar) :- retract(here(_X)), asserta(here(Lugar)).

%La base de conocimiento es estática, por lo tanto no puedo modificar
%el lugar en el que estoy parado. Para que deje de ser estática:

:- dynamic here/1.
:- dynamic tengo/1.
:- dynamic ubicacion/2.

tomar(X) :- puedo_tomar(X), tomar_objeto(X).

puedo_tomar(Objeto) :- here(Lugar), ubicacion(Objeto, Lugar).

tomar_objeto(X) :- retract(ubicacion(X ,_)), asserta(tengo(X)), write('agarrado'), nl.

ubicacion_lista([manzana, vaso, cereales], cocina).
ubicacion_lista([escritorio, computadora], oficina).
ubicacion_lista([luz_escritorio, papeles, lapices], escritorio).
ubicacion_lista(['lampara led', interruptor], luz_escritorio).
ubicacion_lista(['tv smart'], comedor).

eliminar( _ , [ ], [ ]).
eliminar( X, [X | Y], R) :- eliminar( X, Y, R).
eliminar( X, [W| Y], [W | R]) :- X \== W, eliminar( X, Y, R).

sustituir( _ , _ , [ ], [ ]).
sustituir( X, Y, [X | U], [Y | V]) :- sustituir( X, Y, U, V).
sustituir( X, Y, [Z | U], [Z | V]) :- X \== Z, sustituir( X, Y, U, V).

sumar_objeto(Nuevo_Objeto, Contenedor, NuevaLista) :-
ubicacion_lista(Lista_Antigua,Contenedor), append([Nuevo_Objeto],Lista_Antigua,NuevaLista).

sumar_objeto2(Nuevo_Objeto, Contenedor, [Nuevo_Objeto|Lista_Antigua]) :-
ubicacion_lista(Lista_Antigua,Contenedor).

sumar_objeto3(Nuevo_Objeto, Contenedor, NuevaLista) :-
ubicacion_lista(Lista_Antigua,Contenedor), NuevaLista = [Nuevo_Objeto|Lista_Antigua].

:- dynamic ubicacion_lista/2.
poner_objeto(Objeto, Lugar) :- retract(ubicacion_lista(Lista ,Lugar)),
asserta(ubicacion_lista([Objeto|Lista],Lugar)).

% donde_comer(X, Y) :- ubicacion(X, Y), comida(X).
% donde_comer(X, Y) :- ubicacion(X, Y), !, comida(X).
donde_comer(X, Y) :- ubicacion(X, Y), comida(X), !.
